doubleMe x = x + x  --what comes in front of the function name is the parameters

doubleUs x y = x + x + y + y  --this function expects two parameters

doubleUs' x y = doubleMe x + doubleMe y  --you can use the functions that you already have 

next x = succ x
my_min x y = min x y   --the min function only accepts two parameters, not more; however, minimum gets a list as input

doubleSmallNumber x = if x > 100
then x
else x*2   --if statements ALWAYS need an 'else' part in Haskell

doubleSmallNumber' x = (if x > 100 then x else x*2) +1   --the result that is returnes by 'if' is an expression. You can do more stuff on it.

concat x y = x ++ y  --does this work? NOPE. I was not able to use it with two lists as parameters.

boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x ]

rightTriangles' = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2, a+b+c == 24 ]

-- pattern matching

-- it starts checking from the first patter. As soon as it matches a pattern it won't check the rest.

lucky :: (Integral a) => a -> String -- if you try to call it with something other than an Integral, it will give an error
lucky 7 = "Lucky number seven"
lucky x = "Not seven"  -- this is a catch-all pattern. Anything would match


sayMe :: (Integral a ) => a -> String
sayMe 1 = "One"
sayMe 2 = "Two"
sayMe 3 = "Three"
sayMe x = "Not between 1 and 3"


factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial x = x * (factorial x-1)


addVectors :: (Num a) => (a,a) -> (a,a) -> (a,a)
addVectors a b = (fst a + fst b , snd a + snd b)


addVectors' :: (Num a) => (a,a) -> (a,a) -> (a,a)
addVectors' (x1,y1) (x2,y2) = (x1+x2 , y1+y2)



sum_tuple_elements xs = [a+b | (a,b) <- xs]


head' :: [a] -> a
head' [] = error "Cannot call head on empty list"
head' (x:_) = x



tell :: (Show a) => [a] -> String
tell [] = "Empty list"
tell (x:[]) = "List with one element: " ++ show x
tell (x:y:[]) = "List with two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "List is long. The first two elements are: " ++ show x ++ " and " ++  show y

length' ::(Num b) => [a] -> b
length' [] = 0
length'(_:xs) = 1 + length' xs

sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs 


capital :: String -> String
capital "" = "This is an empty string"
capital (x:xs) = "The first letter of " ++ (x:xs) ++ " is " ++ [x]

-- or: capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]   you can use all as an alias for the pattern to avoid repeating it. Useful when the pattern is long and you need to repeat it several times in the function body.

bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
    | bmi <= 18.5 = "You are underweight" 
    | bmi <= 25.0 = "You are normal"
    | bmi <= 30 = "You are overweight"
    | otherwise = "You are obese"


bmiTell' :: (RealFloat a) => a -> a -> String
bmiTell' weight height
    | weight / (height ^ 2) <= 18.5 = "You are underweight"
    | weight / (height ^ 2) <= 25.0 = "You are normal"
    | weight / (height ^ 2 ) <= 30.0 = "You are overweight"
    | otherwise = "You are obese"


max' :: (Ord a) => a -> a -> a
max' a b
    | a > b = a
    | otherwise = b


max'' :: (Ord a) => a -> a -> a
max'' a b | a > b = a | otherwise = b  -- not very readable, but looks cool :D

bmiTell'' :: (RealFloat a) => a -> a -> String
bmiTell'' weight height
    | bmi <= 18.5 = "You are underweight"
    | bmi <= 25.0 = "You are normal"
    | bmi <= 30.0 = "You are overweight"
    | otherwise = "You are obese"
    where bmi = weight / (height ^ 2)  -- to avoid repeating the expression
