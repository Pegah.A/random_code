import tensorflow as tf
import numpy as np

"""
x= tf.constant(35, name = 'x')  # create a constant value called x and give it the value 35
y = tf.Variable(x+5 , name = 'y') # create a variable called y and define it as x+5
print y  # print out the equation object for y   this will NOT print 40
# y is an equation
# the computation of the value y is never performed in the above code


x = tf.constant(35, name = 'x')
y = tf.Variable(x+5, name = 'y')
model = tf.global_variables_initializer() # initialize the variables
# in this step a graph is created of the dependencies between the variables
# in this case the variable y depends on the variable x and that value is transformed by adding 5 to it
# this value is yet not computed until session.run(y)
# before that only the equation and the relations are computed


with tf.Session() as session: # create a session for computing the values
    session.run(model)  # run the model
    print session.run(y) # run just the variable y and print its current value


x = tf.constant([35, 40,45], name = 'x')
y = tf.Variable(x+5, name = 'y')

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    print (session.run(y))


data = np.random.randint(1000, size = 10000)
print "data is: ", data
x = tf.constant(data, name = 'x')
y = tf.Variable(x + 5, name = 'y')

model = tf.global_variables_initializer()
with tf.Session() as session:
    session.run(model)
    print session.run(y)


x = tf.Variable(0, name = 'x')
print "x is: ", x
model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)

    for i in range(5):
        x = x + 1  # we are updating the TensorFlow variable in a loop
        print session.run(x)

x = tf.Variable(0, name = 'x')
print "type of x is: ", type(x)
model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)

    for i in range (5):
        data = np.random.randint(1000)
        avg = np.mean(data)
        print "the average is: ", avg
        x = tf.convert_to_tensor(avg)  # without converting to tensor it will give an error saying that
                                        # it cannot convert float to tensor  because x is a tensor  avg is a float
        print session.run(x)


a = tf.constant(3, name = 'a')
b = tf.constant(4, name = 'b')
add_op = a + b
with tf.Session() as session:
    print session.run(add_op)


a = tf.constant([1,2,3], name = 'a')
b = tf.constant([4,5,6], name = 'b')
add_op = a + b

with tf.Session() as session:
    print session.run(add_op)

a = tf.constant([1,2,3], name = 'a')
b = tf.constant(4, name = 'y')
add_op = a+ b # this is broadcasting  the scalar is added to each element of the list
with tf.Session() as session:
    print session.run(add_op)

a = tf.constant([[1,2,3], [4,5,6]], name = 'a')
b = tf.constant([[1,2,3], [4,5,6]], name = 'b')

add_op = a + b

with tf.Session() as session:
    print session.run(add_op)



a = tf.constant([[1,2,3], [4,5,6]], name = 'a')
b = tf.constant(100, name = 'b')
add_op = a + b

with tf.Session() as session:
    print session.run(add_op)

a = tf.constant([[1,2,3], [4,5,6]], name = 'a')
b = tf.constant([100,101,102], name = 'b')
add_op = a + b  # in this case, the array is getting broadcasted

with tf.Session() as session:
    print session.run(add_op)
    

a = tf.constant([[1,2,3], [4,5,6]], name = 'a')
b = tf.constant([100,101], name = 'b')
add_op = a + b  #this will give an error  because it cannot broadcast properly


with tf.Session() as session:
    print session.run(add_op)
"""

a = tf.constant([[1,2,3], [4,5,6]], name = 'a')
b = tf.constant([[100],[101]], name = 'b')
add_op = a + b  # this will work it will use 100 for the first row and 101 for the second row

print a.shape # (2,3)
print b.shape # (2,1)

# because the first dimensions match, the broadcasting will happen for each row
with tf.Session() as session:
    print session.run(add_op)

