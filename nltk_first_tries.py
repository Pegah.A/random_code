import nltk

#nltk.download()


from nltk.stem import PorterStemmer
from nltk.stem import LancasterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize

# from https://www.datacamp.com/community/tutorials/stemming-lemmatization-python

porter = PorterStemmer()

lancaster = LancasterStemmer()

print ("Porter Stemmer")

print (porter.stem("cats"))
print (porter.stem("trouble"))
print (porter.stem("trouble"))
print (porter.stem("troubled"))


print ("Lancaster Stemmer")

print (lancaster.stem("cats"))
print (lancaster.stem("trouble"))
print (lancaster.stem("troubling"))
print (lancaster.stem("troubled"))

print ("----------")
word_list = ['friend' , 'friendship',
             'friends', 'friendships', 'stabil', 'destabilize', 'misunderstanding', 'railroad',
             'moonlight', 'football']


print ('{0:20}{1:20}{2:20}'.format('Word', 'Porter Stemmer', 'Lancaster Stemmer'))

for word in word_list:
    print ('{0:20}{1:20}{2:20}'.format(word,porter.stem(word), lancaster.stem(word)))


print ("------------")
sentence = "Pythoners are very intelligent and work very pythonly and now they are pythoning their way to success."
print (porter.stem(sentence))

print ("-------------")


def stemSentence(sentence):
    token_words = word_tokenize(sentence)
    stem_sentence = []

    for word in token_words:
        stem_sentence.append(porter.stem(word))
        stem_sentence.append(" ")
    return "".join(stem_sentence)


x = stemSentence(sentence)
print (x)
