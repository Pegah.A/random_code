import PIL
from PIL import Image
import tensorflow as tf
from matplotlib import pyplot as plt

import numpy as np
import os


def rescale_images():
    original_image = Image.open("wb.jpg")
    size = (original_image.size[0], original_image.size[0])  # gives width
    new_image =original_image.resize(size, PIL.Image.ANTIALIAS)

    new_image.save("wb_scaled3.jpg")



def split_images():
    original_image = Image.open("wb.jpg")

    print (original_image.size)

    width, height = original_image.size

    left_box = (0, 0, height, height)  # left, upper, right, lower
    left_slice = original_image.crop(left_box)
    left_slice.save("wb_left.jpg")

    right_box=(256, 0, width, height)
    right_slice = original_image.crop(right_box)
    right_slice.save("wb_right.jpg")



def stitch_images():

    left_image = Image.open("wb_left_p.jpg")
    right_image = Image.open("wb_right_p.jpg")

    (width1, height1) = left_image.size
    (width2, height2) = right_image.size

    result_width = width1 + width2
    result_height = max(height1, height2)

    result = Image.new('RGB', (result_width, result_height))
    result.paste(im=left_image, box=(0, 0))
    result.paste(im=right_image, box=(width1, 0))

    result.save("wb_stitched.jpg")



"""
def resize_image():

    image_size =(128,128)
    scaled_wb_path = "/Users/pegah_abed/Documents/RandomCode/wb_scaled.jpg"
    image_encoded = tf.read_file(scaled_wb_path)
    image_decoded = tf.image.decode_jpeg(image_encoded, channels=3)
    image_resized = tf.image.resize_images(image_decoded, image_size)

    print (type(image_decoded))
    print (type(image_resized))

    return image_resized




def inputs():
    reshaped_image = resize_image()
    return reshaped_image

with tf.Graph().as_default():
    cur_dir = os.getcwd()
    image = inputs()
    init = tf.initialize_all_variables()
    sess = tf.Session()
    sess.run(init)

    #fig, ax = plt.subplots(ncols=1)
    #ax.imshow(sess.run(image))
    #plt.show()

    #print (type(sess.run(image)))


    img = sess.run(image)
    print(img)
    img = Image.fromarray(img, "1")
    print (img)

    plt.imshow(img)
    #plt.show()

    #img.save(os.path.join(cur_dir,"foo.jpeg"))
    #img.show()


"""



rescale_images()

