import numpy as np
import tensorflow as tf

x = np.random.sample((100,2))
# make a dataset from a numpy array
dataset = tf.data.Dataset.from_tensor_slices(x)
# create the iterator
iter = dataset.make_one_shot_iterator()

el = iter.get_next()

with tf.Session() as sess:

    for i in range(100):
        print(sess.run(el))