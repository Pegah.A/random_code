import tensorflow as tf


# Does 'None' mean the number of images in this batch? No, it means all the images in your training set.
# You have had 3 networks that share weights. You have given each image as input to each network and gotten the output.

anchor_output = ""  # shape : [None, 128]  128 is the embedding size
positive_output = ""  # shape : [None, 128]  128 is the embedding size
negative_output = ""  # shape : [None, 128]  128 is the embedding size

margin = 10 # what is a good margin value?

# anchor_output - positive_output shape will be [None, 128]
# anchor_output - negative_output shape will be [None, 128]

# after tf.square the shape will still be [None, 128].
# tf.square works element-wise

# tf.reduce_sum( , 1) will get the sum over axis = 1
# example:
# x = tf.constant([[1, 1, 1], [1, 1, 1]])  This shape is: (2,3)  2 rows, 3 columns
# tf.reduce_sum(x, 1) will be [3,3]  # calculates sum over rows. Outputs 2 values

d_pos = tf.reduce_sum(tf.square(anchor_output - positive_output), 1)  # calculate the distance between anchor and positive
d_neg = tf.reduce_sum(tf.square(anchor_output - negative_output), 1)  # calculate the distance between anchor and negative


loss = tf.maximum(0.0, margin + d_pos - d_neg)
loss = tf.reduce_mean(loss)


# if loss is 0, this is a easy negative
# if 0 < loss < margin, this a semi-hard negative
# if loss > margin, this is a hard negative


