# K-MEANS
# Clustering with the k-means algorithm
# We have a bunch of data and we want to find groups of points that are similar to each other
# You need to tell it how many clusters you want to find: K is as input
# Puts k points (centroids) randomly in your space
# 1. For each x in the data set, find the nearest centroid -> compute the distance to every centroid
# Assign this x to the cluster of the nearest centroid
# 2. Then you loop through each cluster (we have k of those) and reposition it's centre
# The new centre is going to be the mean of all the x points that belong to that cluster
# The attributes need to be numeric because you are taking the average
# If they are categorical or ordinal, what would an average mean for them?
# Keep running these 2 steps one after another until no point changes clustership anymore
# K-means is a fast clustering algorithm
# O ( #iterations * #clusters * #instances * #dimensions)


# Generating Samples
# We are generating three centroids, and then randomly choose (with a normal distribution) around that point.

import numpy as np
import TensorFlow as tf

def create_samples(n_clusters, n_samples_per_cluster, n_features, embiggen_factor, seed):

    np.random.seed(seed)
    slices = []
    centroids = []

    for i in range(n_clusters):
        samples = tf.random_normal((n_samples_per_cluster, n_features),
                                    mean = 0.0, stddev = 5.0, dtype = tf.float32, seed = seed, name = "cluster_{}".format(i))

        # tf.random_normal: Outputs random values from a normal distribution.
        current_centroid = (np.random.random((1,n_features))*embiggen_factor) - (embiggen_factor/2)

