import PIL
from PIL import Image
import cv2
import numpy as np
import os
import sys
import glob
from matplotlib import pyplot as plt

sys.path.append(os.getcwd())
from config import *


def pad_whole_brain_images():
    WHITE = [255, 255, 255]
    whole_brain_img_dir = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(),
                                       "whole_brain", str(MOUSE_WIDTH) + "x" + str(MOUSE_HEIGHT))
    whole_brain_padded_img_dir = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(),
                                              "whole_brain", str(MOUSE_WIDTH) + "x" + str(MOUSE_HEIGHT) + "_padded")

    print ("saving the padded images in: ", whole_brain_padded_img_dir)
    os.chdir(whole_brain_img_dir)

    for file in glob.glob("*.jpg"):
        img = cv2.imread(file)
        padded_image = cv2.copyMakeBorder(img, 0, MOUSE_HEIGHT, 0, 0, cv2.BORDER_CONSTANT, value=WHITE)
        cv2.imwrite(os.path.join(whole_brain_padded_img_dir, file), padded_image)


def rescale_whole_brain_images():
    whole_brain_img_dir = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(),
                                       "whole_brain", str(MOUSE_WIDTH) + "x" + str(MOUSE_HEIGHT))
    whole_brain_scaled_img_dir = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(),
                                              "whole_brain", str(MOUSE_WIDTH) + "x" + str(MOUSE_HEIGHT) + "_rescaled")

    print ("saving the scaled images in: ", whole_brain_scaled_img_dir)

    os.chdir(whole_brain_img_dir)

    for file in glob.glob("*.jpg"):
        original_image = Image.open(file)
        size = (original_image.size[0], original_image.size[0])  # gives width
        scaled_image = original_image.resize(size, PIL.Image.ANTIALIAS)

        scaled_image.save(os.path.join(whole_brain_scaled_img_dir, file))


def split_whole_brain_images():

whole_brain_img_dir = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(),
                                   "whole_brain", str(MOUSE_WIDTH) + "x" + str(MOUSE_HEIGHT))
whole_brain_split_img_dir = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(),
                                         "whole_brain", str(MOUSE_WIDTH) + "x" + str(MOUSE_HEIGHT) + "_splitted")

image_id_text = open("image_IDs.txt", "w")
print ("saving the splitted images in: ", whole_brain_split_img_dir)

os.chdir(whole_brain_img_dir)

for file in glob.glob("*.jpg"):
    image_id = file.split(".")[0]
    image_id_text.write(str(image_id))
    image_id_text.write("\n")

    original_image = Image.open(file)
    width, height = original_image.size

    left_box = (0, 0, height, height)  # left, upper, right, lower
    left_slice = original_image.crop(left_box)
    left_slice_name = image_id + "_left.jpg"
    left_slice.save(os.path.join(whole_brain_split_stitch_img_dir, left_slice_name))

    right_box = (256, 0, width, height)
    right_slice = original_image.crop(right_box)
    right_slice_name = image_id + "_right.jpg"
    left_slice.save(os.path.join(whole_brain_split_stitch_img_dir, right_slice_name))


def stitch_images():
    left_image = Image.open("wb_left_p.jpg")
    right_image = Image.open("wb_right_p.jpg")

    (width1, height1) = left_image.size
    (width2, height2) = right_image.size

    result_width = width1 + width2
    result_height = max(height1, height2)

    result = Image.new('RGB', (result_width, result_height))
    result.paste(im=left_image, box=(0, 0))
    result.paste(im=right_image, box=(width1, 0))

    result.save("wb_stitched.jpg")


split_whole_brain
images()